const mysql = require('mysql');
const Logger = require('../logging/logger')



module.exports = class mysqlDB {

    constructor(host, port, database, username, password) {  // Constructor
        this.pool = connection(host, port, database, username, password);
    }

    static async connection(host, port, database, username, password)
    {
        return await mysql.createPool({
            host     : host,
            port     : port,
            user     : username,
            password : password,
            database : database
          });
    }
/*
    static async insert(payload,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            const result = await db.insertOne(payload);
            
            console.log(`A document was inserted with the _id: ${result.insertedId}`);
        }
        finally {
            await client.close();
        }
    }

    static async updateById(id,field,value,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            let filter = {
                $set:{}
            }
            filter["$set"][field] = value
            const result = await db.updateOne({_id:id},filter);
            
            console.log(`A document was updated with the _id: ${id} [${field}:${value}]`);
        }
        finally {
            await client.close();
        }
    }    
*/
    static async find(table,query,options = {}) {
        try {

            this.pool.getConnection((err, connection) => {
                if(err) throw err;
                Logger.info('[mysql] connected as id ' + connection.threadId);
                connection.query(query, (err, rows) => {
                    // connection.release(); // return the connection to pool
                    if(err) throw err;                    
                    Logger.info('[mysql] rows: ', rows);
                });
            });
        }
        catch(err) {
            Logger.error(`[mysql] query: ${query} - err: ${err}`);
        }
        finally {
            this.pool.release()
        }
    }    


}    