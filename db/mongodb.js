const { MongoClient } = require("mongodb");
const Logger = require('../logging/logger')

// Replace the uri string with your MongoDB deployment's connection string.
const uri = `mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}`

const client = new MongoClient(uri);


module.exports = class mongoDB {

    static async insert(payload,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            const result = await db.insertOne(payload);

            return result.insertedId
            
            // console.log(`A document was inserted with the _id: ${result.insertedId}`);
        }
        finally {
            await client.close();
        }
    }

    static async updateField(id,field,value,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            let filter = {
                $set:{}
            }
            filter["$set"][field] = value
            const result = await db.updateOne({_id:id},filter);

            return (result.modifiedCount==1) ? id : null
            // ${result.matchedCount}
            // console.log(`A document was inserted with the _id: ${id}`);
        }
        finally {
            await client.close();
        }
    } 

    static async update(id,data,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            let filter = {
                $set:{}
            }
            Object.entries(data).forEach(entry => {
                const [key, value] = entry;
                filter["$set"][key] = value
            });
            
            const result = await db.updateOne({_id:id},filter);

            return (result.modifiedCount==1||result.matchedCount==1) ? id : null
            // ${result.matchedCount}
            // console.log(`A document was inserted with the _id: ${id}`);
        }
        finally {
            await client.close();
        }
    }     
    
    static async deleteById(id,collection) {
        try {

            // console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);
            const result = await db.deleteOne({_id:id});

            return (result.deletedCount === 1) ? id : null
            
            // console.log(`A document was deleted with the _id: ${id}`);
        }
        finally {
            await client.close();
        }
    }     

    static async find(collection,query,options = {}) {
        try {

            //console.log(`trying to connect to ${uri}...`);

            await client.connect();
            const database = client.db(process.env.MONGODB_DATABASE);
            const db = database.collection(collection);

            // print a message if no documents were found
            if ((await db.estimatedDocumentCount()) === 0) {                
                //Logger.info('empty collection!');
                return null;
            }  

            let cursor = await db.find(query, options);

            return await cursor.toArray()
        }
        finally {
            await client.close();
        }
    }     


}    