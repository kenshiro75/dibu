const jwt = require("jsonwebtoken");

module.exports = class HelperUser {
    
    static token(email)
    {
        let now = new Date()
        return { 
            token : jwt.sign( { email: email }, process.env.TOKEN_KEY, { expiresIn: process.env.TOKEN_EXPIRES+'h', } ),
            expires :  now.setTime(now.getTime()+(process.env.TOKEN_EXPIRES*60*60*1000))
        }
    }
}