const bcrypt = require('bcrypt')
const Logger = require('../logging/logger')

module.exports = class ConfigAdapter {
 
    static async fromRequestPayloadToModel(payload)
    {
        try
        {
            return {                                
                name:payload.name,
                company:(payload.company!=undefined ? payload.company : null),
                environment: payload.environment,
                server: payload.server,
                database: payload.database,
                connection: payload.connection,
                created_at:(payload.created_at!=undefined ? payload.created_at : new Date().toISOString()),
                updated_at:(payload.updated_at!=undefined ? payload.updated_at : new Date().toISOString()), 
            }

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }
}