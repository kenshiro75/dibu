const bcrypt = require('bcrypt')
const Logger = require('../logging/logger')

module.exports = class UserAdapter {

    static async fromRequestPayloadToModel(payload)
    {
        try
        {
            return {                                
                name:payload.name,
                surname:payload.surname,
                company:(payload.company!=undefined ? payload.company : null),
                email:payload.email.toLowerCase(),
                password: await bcrypt.hash(payload.password, 10),
                token:payload.token,
                token_expire_at:new Date(payload.token_expire_at).toISOString(),
                created_at:(payload.created_at!=undefined ? payload.created_at : new Date().toISOString()),
                updated_at:(payload.updated_at!=undefined ? payload.updated_at : new Date().toISOString()), 
            }

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }
}