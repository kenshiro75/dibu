const bcrypt = require('bcrypt')
const Logger = require('../logging/logger')

module.exports = class ConfigAdapter {

    static async fromRequestPayloadToModel(payload)
    {
        try
        {
            return {                                
                name:payload.name,
                type:payload.type,
                config:payload.config,
                notifications: payload.notifications,
                created_at:(payload.created_at!=undefined ? payload.created_at : new Date().toISOString()),
                updated_at:(payload.updated_at!=undefined ? payload.updated_at : new Date().toISOString()), 
            }

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }
}