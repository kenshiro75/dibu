require('dotenv').config()

module.exports = class Logger {


    static info(message)
    {
        Logger.log('info',message)
    }

    static warning(message)
    {
        Logger.log('warning',message)
    }

    static debug(message)
    {
        Logger.log('debug',message)
    }

    static error(message)
    {
        Logger.log('error',message)
    }

    static log(level,message)
    {
        // config:ERROR level:ERROR
        if (process.env.LOG_LEVEL=='error'&&level!='error')
        {
            return
        }     

        const tm = new Date().toISOString()
        console.log(`[${level}][${tm}] ${message}`);
    }
};