require('dotenv').config()

const crypto = require('crypto');
const Logger = require('../logging/logger')
const mongodb = require('../db/mongodb');
const user = require('../models/user');

const collectionName = 'users'


module.exports = class UserRepository {

    static async save(payload)
    {
        try
        {
            let uuid = crypto.randomUUID()
            let n = new user(
                uuid,
                payload.name,
                payload.surname,
                payload.company,
                payload.email,
                payload.password,
                payload.token,
                payload.token_expire_at,
                payload.created_at,
                payload.updated_at
            )

            // build download
            await mongodb.insert(n,collectionName)

            return uuid

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }

    static async update(user,field,value)
    {
        try
        {
            // build download
            await mongodb.updateField(user.getId(),field,value,collectionName)

            return user.getId()

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }    

    static async findById(id)
    {
        try
        {        
            let result = await mongodb.find(collectionName,{_id:id})

            if(result==null||!result.length)
                return []


            return new user(result[0]._id, 
                            result[0].name,
                            result[0].surname,
                            result[0].company,
                            result[0].email,
                            result[0].password,
                            result[0].token,
                            result[0].token_expire_at,
                            result[0].created_at,
                            result[0].updated_at
                        )

        }
        catch(err)
        {
            Logger.error(err)            
        }
    }  
    
    static async findBy(field, value)
    {
        try
        {                
            let result = await mongodb.find(collectionName,{[field]:value})

            if(result==null||!result.length)
                return []

            return new user(result[0]._id, 
                            result[0].name,
                            result[0].surname,
                            result[0].company,
                            result[0].email,
                            result[0].password,
                            result[0].token,
                            result[0].token_expire_at,
                            result[0].created_at,
                            result[0].updated_at
                )

        }
        catch(err)
        {
            Logger.error('findBy error: '+err)            
        }        
    }  
    
    static async findByQuery(query)
    {
        try
        {                
            let result = await mongodb.find(collectionName,query)

            if(result==null||!result.length)
                return []

            return new user(result[0]._id, 
                            result[0].name,
                            result[0].surname,
                            result[0].company,
                            result[0].email,
                            result[0].password,
                            result[0].token,
                            result[0].token_expire_at,
                            result[0].created_at,
                            result[0].updated_at
                )

        }
        catch(err)
        {
            Logger.error('findBy error: '+err)            
        }        
    }      

    /**
     * 
     * @returns Array[download]
     */
    static async findAll()
    {
        try
        {        
            let result = await mongodb.find(collectionName,{})

            if(result==null||!result.length)
                return []

            let collection = []
            
            result.forEach((record)=>{
                collection.push( 
                    new user(record._id, 
                            record.name,
                            record.surname,
                            record.company,
                            record.email,
                            record.password,
                            record.token,
                            record.token_expire_at,
                            record.created_at,
                            record.updated_at
                    )
                )
            })

            return collection

        }
        catch(err)
        {
            Logger.error(err)            
        }
    }   
    
    /**
     * 
     * @returns Array[download]
     */
     static async findAllBy(field, value)
     {
         try
         {        
             let result = await mongodb.find(collectionName,{[field]:value})
 
             if(result==null||!result.length)
                 return []
 
             let collection = []
             
             result.forEach((record)=>{
                 collection.push( 
                     new user(record._id, 
                            record.name,
                            record.surname,
                            record.company,
                            record.email,
                            record.password,
                            record.token,
                            record.token_expire_at,
                            record.created_at,
                            record.updated_at
                            )
                 )
             })
 
             return collection
 
         }
         catch(err)
         {
             Logger.error(err)            
         }
     }      
}