require('dotenv').config()

const crypto = require('crypto');
const Logger = require('../logging/logger')
const mongodb = require('../db/mongodb');
const config = require('../models/config');

const collectionName = 'configs'

 
module.exports = class ConfigRepository {

    static async save(payload)
    {
        try
        {
            let uuid = crypto.randomUUID()
            let n = new config(
                    uuid,
                    payload.name,
                    payload.company,
                    payload.environment,
                    payload.server,
                    payload.database,
                    payload.connection,
                    payload.created_at,
                    payload.updated_at
            )

            // build download
            await mongodb.insert(n,collectionName)

            return uuid

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }

    static async update(config,data)
    {
        try
        {
            let uuid = config.getId()

            let result = await mongodb.update(uuid,data,collectionName)

            return result            

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }  

    static async updateField(config,field,value)
    {
        try
        {
            // build download
            await mongodb.updateField(config.getId(),field,value,collectionName)

            return config.getId()

        }
        catch(err)
        {
            Logger.error(err)
            return null
        }
    }    

    static async findById(id)
    {
        try
        {        
            let result = await mongodb.find(collectionName,{_id:id})

            if(result==null||!result.length)
                return []


            return new config(result[0]._id, 
                            result[0].name,
                            result[0].company,
                            result[0].environment,
                            result[0].server,
                            result[0].database,
                            result[0].connection,
                            result[0].created_at,
                            result[0].updated_at
                        )

        }
        catch(err)
        {
            Logger.error(err)            
        }
    }  
    
    static async findBy(field, value)
    {
        try
        {                
            let result = await mongodb.find(collectionName,{[field]:value})

            if(result==null||!result.length)
                return []

            return new config(result[0]._id, 
                            result[0].name,
                            result[0].company,
                            result[0].environment,
                            result[0].server,
                            result[0].database,
                            result[0].connection,
                            result[0].created_at,
                            result[0].updated_at
                )

        }
        catch(err)
        {
            Logger.error('findBy error: '+err)            
        }        
    }  
    
    static async findByQuery(query)
    {
        try
        {                
            let result = await mongodb.find(collectionName,query)

            if(result==null||!result.length)
                return []

            return new config(result[0]._id, 
                            result[0].name,
                            result[0].company,
                            result[0].environment,
                            result[0].server,
                            result[0].database,
                            result[0].connection,
                            result[0].created_at,
                            result[0].updated_at        
                )

        }
        catch(err)
        {
            Logger.error('findBy error: '+err)            
        }        
    }      

    /**
     * 
     * @returns Array[download]
     */
    static async findAll()
    {
        try
        {        
            let result = await mongodb.find(collectionName,{})

            if(result==null||!result.length)
                return []

            let collection = []
            
            result.forEach((record)=>{
                collection.push( 
                    new config(record._id, 
                                record.name,
                                record.company,
                                record.environment,
                                record.server,
                                record.database,
                                record.connection,
                                record.created_at,
                                record.updated_at
                    )
                )
            })

            return collection

        }
        catch(err)
        {
            Logger.error(err)            
        }
    }   
    
    /**
     * 
     * @returns Array[download]
     */
     static async findAllBy(field, value)
     {
         try
         {        
             let result = await mongodb.find(collectionName,{[field]:value})
 
             if(result==null||!result.length)
                 return []
 
             let collection = []
             
             result.forEach((record)=>{
                 collection.push( 
                     new config(record._id, 
                                record.name,
                                record.company,
                                record.environment,
                                record.server,
                                record.database,
                                record.connection,
                                record.created_at,
                                record.updated_at
                            )
                 )
             })
 
             return collection
 
         }
         catch(err)
         {
             Logger.error(err)            
         }
     }   
     
     static async deleteById(id)
     {
         try
         {        
             let result = await mongodb.deleteById(id,collectionName)
 
             return result
 
         }
         catch(err)
         {
             Logger.error(err)   
             
             return null         
         }
     }        
}