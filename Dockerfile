# syntax = docker/dockerfile:1.2

ARG NODE_VERSION=16.14.0


# Multistage - BASE
FROM node:${NODE_VERSION} as base
ARG UID=1001
ARG GID=1001
ARG USER=node
LABEL "com.example.vendor"="7stars"
LABEL description="Dibu microservice"

#RUN addgroup --gid ${GID} ${USER} && adduser ${USER} --gid ${GID}
RUN mkdir /app && chown -R $USER:$USER /app
USER $USER
WORKDIR /app


FROM base as dev
COPY --chown=app:app .docker/env/dev.env /app/.env
COPY --chown=app:app .docker/utils/mongodb-database-tools-debian10-x86_64-100.7.3.deb /app/mongodb-database-tools-debian10-x86_64-100.7.3.deb
RUN npm install nodemon --save-dev
USER root
RUN apt update
RUN apt install -y mariadb-client
WORKDIR /app
RUN apt install ./mongodb-database-tools-debian10-x86_64-100.7.3.deb
USER $USER
EXPOSE 3000 9229
CMD ["tail", "-f", "/dev/null"]


FROM base as prod
WORKDIR /app
COPY --chown=app:app ./ /app
COPY --chown=app:app .docker/env/prod.env /app/.env
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]