'use strict';

const express = require('express');


// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();

app.use(
    express.urlencoded({
      extended: true
    })
  )
  
app.use(express.json())

// API ROUTES
const routes = require('./routes/api');

// ROUTES
routes(app);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);