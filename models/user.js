module.exports = class User {

    constructor(
        id,
        name,
        surname,
        company,
        email,
        password,
        token,
        token_expire_at,
        created_at,
        updated_at
        )
    {
        this._id = id
        this.name=name,
        this.surname=surname,
        this.company=company,
        this.email=email,
        this.password=password,    
        this.token=token,    
        this.token_expire_at=token_expire_at,    
        this.created_at=created_at,
        this.updated_at=updated_at

    }

    getId(){ return this._id }

    getName(){ return this.name }
    setName(name){ this.name = name }

    getSurname(){ return this.surname }
    setSurname(surname){ this.surname = surname }  
    
    getCompany(){ return this.company }
    setCompany(company){ this.company = company }   

    getEmail(){ return this.email }
    setEmail(email){ this.email = email }       
    
    getPassword(){ return this.password }
    setPassword(password){ this.password = password }           

    getToken(){ return this.token }
    setToken(token){ this.token = token } 
    
    getTokenExpireAt(){ return this.token_expire_at }
    getTokenExpireAt(token_expire_at){ this.token_expire_at = token_expire_at } 

    getCreatedAt(){ return this.created_at }
    setCreatedAt(created_at){ this.created_at = created_at }    
    
    getUpdatedAt(){ return this.updated_at }
    setUpdatedAt(updated_at){ this.updated_at = updated_at }      
           
    
    toObj()
    {
        return {
            id: this.getId(),
            name: this.getName(),
            surname: this.getSurname(),
            company: this.getCompany(),
            email: this.getEmail(),
            password: this.getPassword(),        
            token: this.getToken(),
            token_expire_at: this.getTokenExpireAt(),
            created_at: this.getCreatedAt(),
            updated_at: this.getUpdatedAt()
        }
    }

}


