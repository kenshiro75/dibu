module.exports = class Config {

    constructor(
        id,
        name,
        company,
        environment,
        server,
        database,
        connection,
        created_at,
        updated_at
        )
    {
        this._id = id
        this.name=name,
        this.company=company,
        this.environment=environment,
        this.server=server,
        this.database=database,
        this.connection=connection,
        this.created_at=created_at,
        this.updated_at=updated_at

    }

    getId(){ return this._id }

    getName(){ return this.name }
    setName(name){ this.name = name }

    getServer(){ return this.server }
    setServer(server){ this.server = server }  
    
    getDatabase(){ return this.database }
    setDatabase(database){ this.database = database }   

    getConnection(){ return this.connection }
    setConnection(connection){ this.connection = connection }               
     
    getCreatedAt(){ return this.created_at }
    setCreatedAt(created_at){ this.created_at = created_at }    
    
    getUpdatedAt(){ return this.updated_at }
    setUpdatedAt(updated_at){ this.updated_at = updated_at }      
           
    
    toObj()
    {
        return {
            id: this.getId(),
            name: this.getName(),
            server: this.getServer(),
            database: this.getDatabase(),
            connection: this.getConnection(),
            created_at: this.getCreatedAt(),
            updated_at: this.getUpdatedAt()
        }
    }

}


