module.exports = class Config {

    constructor(
        id,
        name,
        type,
        config,
        notifications,
        created_at,
        updated_at
        )
    {
        this._id = id
        this.name=name,
        this.type=type,
        this.config=config,
        this.notifications=notifications,
        this.created_at=created_at,
        this.updated_at=updated_at

    }

    getId(){ return this._id }

    getName(){ return this.name }
    setName(name){ this.name = name }

    getType(){ return this.type }
    setType(type){ this.type = type }    

    getConfig(){ return this.config }
    setConfig(config){ this.config = config }  
    
    getNotifications(){ return this.notifications }
    setNotifications(notifications){ this.notifications = notifications }   

    getConnection(){ return this.connection }
    setConnection(connection){ this.connection = connection }               
     
    getCreatedAt(){ return this.created_at }
    setCreatedAt(created_at){ this.created_at = created_at }    
    
    getUpdatedAt(){ return this.updated_at }
    setUpdatedAt(updated_at){ this.updated_at = updated_at }      
           
    
    toObj()
    {
        return {
            id: this.getId(),
            name: this.getName(),
            server: this.getServer(),
            database: this.getDatabase(),
            connection: this.getConnection(),
            created_at: this.getCreatedAt(),
            updated_at: this.getUpdatedAt()
        }
    }

}


