'use strict';

const auth = require("../middleware/auth");

const aboutController = require('../controllers/aboutController');
const authController = require('../controllers/authController');
const userController = require('../controllers/userController');
const configController = require('../controllers/configController');
const backupController = require('../controllers/backupController');


module.exports = (app) => {
    
    app.route('/v1/about').get(aboutController.about);

    // AUTH
    app.route('/v1/auth/register').post(authController.register);
    app.route('/v1/auth/login').get(authController.login);

    // USERS
    app.route('/v1/users').get(auth, userController.list);

    // CONFIG
    app.route('/v1/config').get(auth, configController.list);
    app.route('/v1/config').put(auth, configController.save);
    app.route('/v1/config').post(auth, configController.save);
    app.route('/v1/config/:uuid').delete(auth, configController.delete);

    // BACKUP
    app.route('/v1/backup').get(auth, backupController.list);
    app.route('/v1/backup').put(auth, backupController.save);
    app.route('/v1/backup').post(auth, backupController.save);
    app.route('/v1/backup/:uuid').patch(auth, backupController.execute);
    app.route('/v1/backup/:uuid').delete(auth, backupController.delete);

}