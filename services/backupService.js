
const Client = require('ssh2').Client;
const { spawn } = require('child_process');
const Logger = require('../logging/logger')
const backupRepository = require('../repositories/backupRepository')
const configRepository = require('../repositories/configRepository')
const mysqlService = require('./mysqlService')
const mongoService = require('./mongoService');
const { restoreCmd } = require('./mysqlService');

module.exports = class BackupService {
  /*

  # Backup
docker exec CONTAINER /usr/bin/mysqldump -u root --password=root DATABASE > backup.sql

# Restore
cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE

*/

  static async connect(config)
  {
    
    Logger.info(`Connecting to ${config.host}:${config.port}`)
    try {
      const client = new Client()
      return new Promise((resolve, reject) => { 
        client.connect(config).on('ready',()=>{
          Logger.info(`Connected to ${config.host}:${config.port}`)
          resolve(client)
        })
      })
    } catch (err) {
      Logger.error('Failed to connect:', err);
    }    

  }

  static async remoteExec(client,cmd)
  {
    
    Logger.info(`executing ${cmd}`)
 
    return new Promise((resolve, reject) => { 
      client.exec(cmd, (err, stream) => {
        if (err) 
        {
          reject(err)
        }
        stream.on('close', (code, signal) => {
          Logger.info(`command finished with code: ${code} signal: ${signal}`);
              // client.end();
              resolve()
        }).on('data', (data) => {
          Logger.info(`STDOUT: ${data}`);
        }).stderr.on('data', (data) => {
          Logger.info(`STDERR: ${data}`);
        });
      })
    })
  }

  static async localExec(cmd)
  {
    Logger.info(`executing ${cmd}`)
 
    return new Promise((resolve, reject) => { 

      const process = spawn('sh', ['-c',cmd]);
      process.on('close', function (code) { // Should probably be 'exit', not 'close'
        // *** Process completed
        resolve(code);
        return 'ok'
      });
      process.on('error', function (err) {
        // *** Process creation failed
        Logger.error(`spawn error: ${err}`)
        reject(err);
        return null
      });  
           
    })
  }

  static async sftpDownload(client,remotePath,localPath)
  {

    Logger.info(`copying via sftp : ${remotePath} to ${localPath}`)
 
    return new Promise((resolve, reject) => { 
      client.sftp((err, sftp) => {
        if (err) 
        {
          reject(err)
        }
        sftp.fastGet(remotePath, localPath, (downloadErr) => {
            if (downloadErr) 
            {
              reject(downloadErr)
            }
            Logger.info('Download complete');
            resolve()
        })
      })   
    }) 
  }

  static async sftpUpload(client,localPath,remotePath)
  {

    Logger.info(`copying via sftp : ${localPath} to ${remotePath}`)
 
    return new Promise((resolve, reject) => { 
      client.sftp((err, sftp) => {
        if (err) 
        {
          reject(err)
        }
        sftp.fastPut(localPath, remotePath, (uploadErr) => {
            if (uploadErr) 
            {
              reject(uploadErr)
            }
            Logger.info('Upload complete');
            resolve()
        })
      })   
    }) 
  }

  static async createRemoteDB(client,config)
  {
    let cmd = null
    switch(config.database.type)
    {
      case 'mysql':
        cmd = mysqlService.createDBCmd(config)
      break;
      case 'mongo':
        cmd = mongoService.createDBCmd(config)
      break;
    }

    if(cmd==null)
    {
      return null
    }  

    return await BackupService.remoteExec(client,cmd)
  }

  static dumpFileName(config, path)
  {
    let dumpFileName = null

    switch(config.database.type)
    {
      case 'mysql':
        dumpFileName = mysqlService.dumpFileName(path, config.connection.database)
      break;
      case 'mongo':
        dumpFileName = mongoService.dumpFileName(path, config.connection.database)
      break;
    }

    return dumpFileName
  }  

  static async dump(backup)
  {
    let src = await configRepository.findById(backup.config.source)
    let dst = await configRepository.findById(backup.config.destination[0])

    let srcDumpFileName = BackupService.dumpFileName(src,src.server.dump.path)
    let dstDumpFileName = BackupService.dumpFileName(src,dst.server.dump.path)

    if(src.connection.ssh==null)
    {
      srcDumpFileName = dstDumpFileName
    }

    let cmd = null

    switch(src.database.type)
    {
      case 'mysql':
       cmd = mysqlService.dumpCmd(src,srcDumpFileName)
      break;
      case 'mongo':
       cmd = mongoService.dumpCmd(src,srcDumpFileName)
      break;
    }

    if(cmd==null)
    {
      return null
    }     

    if(src.connection.ssh!=null)
    {

      let source = {
        host: src.connection.ssh.ip,
        port: src.connection.ssh.port,
        username: src.connection.ssh.user,
        password: src.connection.ssh.password,
        //privateKey: require('fs').readFileSync('~/.ssh/id_rsa')
      }

      let srcConn = await BackupService.connect(source)

      // dump from source
      await BackupService.remoteExec(srcConn,cmd)
  
      // copy from source to local
      await BackupService.sftpDownload(srcConn,srcDumpFileName,dstDumpFileName)

      // source connection close
      srcConn.end()  
    }
    else {
      await BackupService.localExec(cmd)
    }

    return dstDumpFileName

  }

  static async restore(backup,dumpFileName)
  {

    let dst = await configRepository.findById(backup.config.destination[0])

    let destCmd = null
    switch(dst.database.type)
    {
      case 'mysql':
        destCmd = mysqlService.restoreCmd(dst,dumpFileName)
      break;
      case 'mongo':
        destCmd = mongoService.restoreCmd(dst,dumpFileName)
      break;
    }

    if(destCmd==null)
    {
      return null
    }  
    
    if(dst.connection.ssh!=null)
    {
      let destination = {
        host: dst.connection.ssh.ip,
        port: dst.connection.ssh.port,
        username: dst.connection.ssh.user,
        password: dst.connection.ssh.password,
        //privateKey: require('fs').readFileSync('~/.ssh/id_rsa')      
      }      

      // connect to destination
      let destConn = await BackupService.connect(destination)

      // copy from local to destination
      await BackupService.sftpUpload(destConn,dumpFileName,dumpFileName)

      // create db on destination
      await BackupService.createRemoteDB(destConn,dst)

      // restore db on destination
      await BackupService.remoteExec(destConn,destCmd)

      // remove file from destination
      
      // destination connection close
      destConn.end()

      return 'ok'
    }
    else {
      return await BackupService.localExec(destCmd)
    }

  }
    
  static async dumpAndRestore(backup)
  {

    let src = await configRepository.findById(backup.config.source)
    let dst = await configRepository.findById(backup.config.destination[0])

    let dumpFileName = await BackupService.dump(backup)
    return await BackupService.restore(backup,dumpFileName)

  }
  
}
