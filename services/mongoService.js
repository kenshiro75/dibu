
const Logger = require('../logging/logger')


module.exports = class MongoService {

  static async createDBCmd(config)
  { 
    let cmd = null

    return cmd
  }

  static dumpFileName(path,database)
  {
    let currentDate = new Date().toISOString().replaceAll('-','').replaceAll(':','').replace('T','_').split('.')[0]
    return path + database + '_' + currentDate
  }

  static dumpCmd(config, dumpFileName)
  {
    let cmd = null

    if(config.server.type=='docker')
    {
      cmd = `sudo sh -c 'docker exec $(docker ps --format "{{.Names}}" | grep ${config.server.container.name}) /usr/bin/mongodump -d ${config.connection.database} -u ${config.connection.user} -p ${config.connection.password} --port ${config.connection.port} --authenticationDatabase admin --gzip --archive=${dumpFileName} ; docker cp $(docker ps --format "{{.Names}}" | grep ${config.server.container.name}):${dumpFileName} ${dumpFileName}'`
    }
    else {
      cmd = `mongodump -d ${config.connection.database} -u ${config.connection.user} -p ${config.connection.password} --port ${config.connection.port} --authenticationDatabase admin`
    }

    return cmd
  }

  static restoreCmd(config, dumpFileName)
  {
    let cmd = null

    if(config.server.type=='docker')
    {
      cmd = `sh -c 'cat ${dumpFileName} | docker exec -i $(docker ps --format "{{.Names}}" | grep ${config.server.container.name}) /usr/bin/mongorestore -u ${config.connection.user} -p ${config.connection.password} --port ${config.connection.port} --authenticationDatabase admin --gzip --archive=${dumpFileName} --drop'`
    }
    else {
      cmd = `/usr/bin/mongorestore -u ${config.connection.user} -p ${config.connection.password} -h ${config.connection.ip} --port ${config.connection.port} --authenticationDatabase admin --gzip --archive=${dumpFileName} --drop`
    }

    return cmd
  }

}
