
const Logger = require('../logging/logger')


module.exports = class MysqlService {

  static createDBCmd(config)
  { 
    let cmd = null

    if(config.server.type=='docker')
    {
      cmd = "docker exec $(docker ps --format \"{{.Names}}\" | grep "+config.server.container.name+") /usr/bin/mysql -u"+config.connection.user+" -p"+config.connection.password+" -e 'create database `"+config.connection.database+"`;'"
    }
    else {
      cmd = "/usr/bin/mysql -u"+config.connection.user+" -p"+config.connection.password+" -e 'create database `"+config.connection.database+"`;'"
    }

    return cmd
  }

  static dumpFileName(path,database)
  {
    let currentDate = new Date().toISOString().replaceAll('-','').replaceAll(':','').replace('T','_').split('.')[0]
    return path + database + '_' + currentDate + '.sql'
  }

  static dumpCmd(config, dumpFileName)
  {
    let cmd = null

    if(config.server.type=='docker')
    {
      cmd = `docker exec $(docker ps --format "{{.Names}}" | grep ${config.server.container.name}) /usr/bin/mysqldump -u${config.connection.user} -p${config.connection.password} ${config.connection.database} > ${dumpFileName}`
    }
    else {
      cmd = `mysqldump -u ${config.connection.user} -p ${config.connection.password} --host ${config.connection.ip} ${config.connection.database} > ${dumpFileName}`
    }

    return cmd
  }

  static restoreCmd(config, dumpFileName)
  {
    let cmd = null

    if(config.server.type=='docker')
    {
      cmd = `cat ${dumpFileName} | docker exec -i $(docker ps --format "{{.Names}}" | grep ${config.server.container.name}) /usr/bin/mysql -u${config.connection.user} -p${config.connection.password} ${config.connection.database}`
    }
    else {
      cmd = `mysql -u ${config.connection.user} -p ${config.connection.password} --host ${config.connection.ip} ${config.connection.database} < ${dumpFileName}`
    }

    return cmd
  }

}
