'use strict';

const bcrypt = require('bcrypt') 

const Logger = require('../logging/logger')

const jwt = require("jsonwebtoken");
const UserRepository = require('../repositories/userRepository');
const UserHelper = require('../helpers/userHelper');
const userAdapter = require('../adapters/userAdapter')



var controllers = {
    async register(req, res, next)
    {
        try
        {
            let payload = req.body
    
            let t = UserHelper.token(payload.email)
            payload.token = t.token
            payload.token_expire_at = t.expires 
    
            let adapted = await userAdapter.fromRequestPayloadToModel(payload)
            let result = await UserRepository.save(adapted)

            if(result==null)
            {
                throw new Error('there was a problem creating the user!')
            }
    
            res.json({status:'ok', data: {id: result}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async login(req, res, next)
    {
        try
        {
            const { email, password } = req.body

            if (!(email && password)) 
            {
                throw new Error("All input is required")
            }

            let user = await UserRepository.findBy('email',email);
            if(user._id==undefined)
            {
                throw new Error("Invalid Credentials")
            }

            if (await bcrypt.compare(password, user.getPassword())) {
                // Create token
                let t = UserHelper.token(email)

                // save user token
                await UserRepository.update(user,'token',t.token);
                await UserRepository.update(user,'token_expires_at',new Date(t.expires).toISOString());

                t.expires = new Date(t.expires).toISOString()
                res.json({status:'ok', data: t});
            }
            else {
                throw new Error("Invalid Password")
            }
            
        }
        catch(err)
        {
            res.status(403).json({status:'ko', error: err.message})
        }
    },



};

module.exports = controllers;