'use strict';

const properties = require('../package.json');

var controllers = {
    about: (req, res) => {
        var aboutInfo = {
            name: properties.name,
            version: properties.version,
        }
        res.json(aboutInfo);
    }

};

module.exports = controllers;