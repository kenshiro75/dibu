'use strict';
 
const Logger = require('../logging/logger')
const backupRepository = require('../repositories/backupRepository')
const backupAdapter = require('../adapters/backupAdapter')
const backupService = require('../services/backupService')

const bcrypt = require('bcrypt')


var controllers = {
 
    async save(req, res, next)
    {
        try
        {
            let uuid = null
            if(req.body.uuid)
            {                
                let data = []
                Object.keys(req.body).forEach(key => {
                    if(key!='uuid')
                    {
                        data[key]= req.body[key]
                    }                    
                  });
                uuid = await backupRepository.update(await backupRepository.findById(req.body.uuid),data)
            }
            else {
                uuid = await backupRepository.save(await backupAdapter.fromRequestPayloadToModel(req.body))                        
            }                        
            
            if (uuid==null)
            {
                throw new Error('there was a problem!')
            }
    
            res.json({status:'ok', data: {id: uuid}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async list(req, res, next)
    {
        try
        {
    
            let backups = await backupRepository.findAll()
           

            if(backups==null)
            {
                throw new Error('there was a problem getting the users!')
            }
    
            res.json({status:'ok', data: backups})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async execute(req, res, next)
    {
        try
        {
            let backup = await backupRepository.findById(req.params.uuid)         
            let result = null           
            switch(backup.type)
            {
                case "dump": 
                    result = await backupService.dump(backup)
                break;
                case "dumpAndRestore": 
                    result = await backupService.dumpAndRestore(backup)
                break;                
            }
            

            if(result==null)
            {
                throw new Error('there was a problem executing the backup!')
            }
    
            res.json({status:'ok', data: {dump: result}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async delete(req, res, next)
    {
        try
        {
            let result = await backupRepository.deleteById(req.params.uuid)                                

            if(result==null)
            {
                throw new Error('there was a problem deleting the backup!')
            }
    
            res.json({status:'ok', data: {id: result}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

};

module.exports = controllers;