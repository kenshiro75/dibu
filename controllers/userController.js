'use strict';
 
const Logger = require('../logging/logger')
const userRepository = require('../repositories/userRepository')

const bcrypt = require('bcrypt')


var controllers = {
    async list(req, res, next)
    {
        try
        {
    
            let users = await userRepository.findAll()
           

            if(users==null)
            {
                throw new Error('there was a problem getting the users!')
            }
    
            res.json({status:'ok', data: users})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

};

module.exports = controllers;