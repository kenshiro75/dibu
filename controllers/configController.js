'use strict';
 
const Logger = require('../logging/logger')
const configRepository = require('../repositories/configRepository')
const configAdapter = require('../adapters/configAdapter')

const bcrypt = require('bcrypt')


var controllers = {

    async save(req, res, next)
    {
        try
        {
            let uuid = null
            if(req.body.uuid)
            {                
                let data = []
                Object.keys(req.body).forEach(key => {
                    if(key!='uuid')
                    {
                        data[key]= req.body[key]
                    }                    
                  });
                uuid = await configRepository.update(await configRepository.findById(req.body.uuid),data)
            }
            else {
                uuid = await configRepository.save(await configAdapter.fromRequestPayloadToModel(req.body))                        
            }                        
            
            if (uuid==null)
            {
                throw new Error('there was a problem!')
            }
            
    
            res.json({status:'ok', data: {id: uuid}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async list(req, res, next)
    {
        try
        {
    
            let configs = await configRepository.findAll()
           

            if(configs==null)
            {
                throw new Error('there was a problem getting the users!')
            }
    
            res.json({status:'ok', data: configs})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

    async delete(req, res, next)
    {
        try
        {
            let result = await configRepository.deleteById(req.params.uuid)                                

            if(result==null)
            {
                throw new Error('there was a problem deleting the config!')
            }
    
            res.json({status:'ok', data: {id: result}})
        }
        catch(err)
        {
            res.status(500).json({status:'ko', error: err.message})
        }

    },

};

module.exports = controllers;