## Name
Dibu

## Description
A simple Microservice written in NodeJs who makes direct and scheduled dumps of dbs.

## Getting started

```
cd <your directory>
git clone https://gitlab.com/kenshiro75/dibu.git
sudo docker-compose build
sudo docker-compose up -d
```

## Dev environment

```
sudo docker exec -ti dibu_node bash
npm install
./node_modules/nodemon/bin/nodemon.js --inspect=0.0.0.0 server.js 
```

## Check is working!

from browser : http://localhost:146/about

## Test and Deploy

....


## REFS
### auth
- https://www.section.io/engineering-education/how-to-build-authentication-api-with-jwt-token-in-nodejs/

### db dumps
- https://howto.webarea.it/mysql/script-per-realizzare-un-backup-automatico-dei-dati-di-mysql-utilizzando-mysqldump-in-bash-e-in-php_43
- https://gist.github.com/spalladino/6d981f7b33f6e0afe6bb

## Authors and acknowledgment
Author: Ettore Cirillo - kenshiro1@libero.it

## License
This project is open source undr MIT license

## Project status


mongodump -d pabu -o . -u root -p mongo --authenticationDatabase admin